---
sidebar: auto
---

# PIA
## Group Overview

The Pinewood Intelligence Agency, owned and managed by Diddleshot, performs special duties at Pinewood Builders games, which can be such things as specialist security details, exploiter patrol, and moderation. Joining the Pinewood Intelligence Agency is limited to invite only.

P.I.A has been known to have been issued with various advanced weapons over the course of Pinewood Builders history, most notably and iconically the explosive pen. Agents are typically comprised of high-ranking Pinewood personnel, although there are a number of current exceptions to this such as Erika1942.

## P.I.A Staff
### Head of Intelligence:	
- Diddleshot  

### Alpha Level Agent:	
- LENEMAR, 
- Csdi, 
- Irreflexive  

### Agent:	
- WickyTheUnicorn, 
- TheGreatOmni, 
- TenX29, 
- Erika1942, 
- RogueVader1996, 
- Vah_Medoh,
- Supremo_miguel

### Intern:	
- none

### On Hiatus:	
- None
